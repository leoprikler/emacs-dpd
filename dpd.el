;;; dpd.el

;; Filename: dpd.el
;; Description: Deliver packages to package.el
;; Author: Liliana Marie Prikler
;; Maintainer: Liliana Marie Prikler
;; Created: 10 October 2018
;; Last-Updated: 10 December 2021
;; Version: 0.2.1
;; Package-Requires: (packed)

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>

;;; Commentary:

;; DPD adds a way of systematically providing "external" packages to package.el
;; without depending on any particular package manager.  It uses packed to
;; recognize packages and lisp-mnt to generate their descriptions.

;;; Code:

(require 'lisp-mnt)
(require 'package)
(require 'packed)

;;;###autoload
(defgroup dpd nil
  "Delivering packages to package.el."
  :prefix "dpd"
  :group 'maint)

;;;###autoload
(defcustom dpd-roots
  '()
  "Directories to scan for undelivered packages."
  :type '(repeat directory))

;;;###autoload
(defcustom dpd-recognize-hook
  '(dpd-default-recognize
    dpd-recognize-auctex)
  "Hook to recognize the package within a given directory."
  :type 'hook)

;;;###autoload
(defun dpd (&rest roots)
  "Add all packages inside ROOTS to `package-alist'.
If ROOTS are nil, use `dpd-roots' instead.
When called interactively, prompt for a single root directory."
  (interactive "DRoot of the packages to deliver: ")
  (dolist (dir (or roots dpd-roots) package-alist)
    (dolist (pkg (dpd-find-packages dir))
      (let* ((name (package-desc-name pkg))
             (version (package-desc-version pkg))
             (old-pkgs (assq name package-alist))
             (new-pkg-desc pkg))
        (if (null old-pkgs)
            (push (list name new-pkg-desc) package-alist)
          (while
              (if (and (cdr old-pkgs)
                       (or (not version)
                           (version-list-< version
                                           (package-desc-version
                                            (cadr old-pkgs)))))
                  (setq old-pkgs (cdr old-pkgs))
                (push new-pkg-desc (cdr old-pkgs))
                nil)))))))

(defun dpd-find-packages (dir)
  "Find packages inside DIR and resolve them to package descriptions."
  (let* (packages
         (paths (packed-load-path dir))
         (progress (make-progress-reporter "Finding packages...")))
    (dolist (path paths
                  (progn
                    (progress-reporter-done progress)
                    packages))
      (let ((pkg (run-hook-with-args-until-success
                  'dpd-recognize-hook
                  path)))
        (when (and pkg (package-desc-p pkg))
          (push pkg packages))
        (progress-reporter-update progress)))))

(defun dpd--default-package-for-file (file dir)
  (lm-with-file file
    (package-desc-create
     :name (intern (file-name-base file))
     :version
     (let ((version (or (lm-version)
                        (lm-header "Package-Version"))))
       (and version (version-to-list version)))
     :summary (lm-summary)
     :reqs (let* ((reqs (lm-header-multiline "Package-Requires"))
                  (reqs* (and reqs (read (mapconcat #'identity reqs " ")))))
             (dolist (r reqs* reqs*)
               (when (consp r)
                 (setcdr r (list (version-to-list (cadr r)))))))
     :dir dir)))

(defun dpd-default-recognize (dir)
  "Recognize the main library of DIR, as given by `packed-main-library'."
  (let* ((file (packed-main-library dir nil t)))
    (and file (dpd--default-package-for-file file dir))))

(defun dpd-file--no-load-suffix (file)
  (let (nosuffix)
    (dolist (suffix (get-load-suffixes) (or nosuffix file))
      (when (string-suffix-p suffix file)
        (setq nosuffix (substring file 0
                                  (- (length file)
                                     (length suffix))))))))

(defun dpd-fuzzy-filter (dir libraries)
  "Find among LIBRARIES the one with the longest prefix matching DIR."
  (let (prefix matched-lib (key (file-name-nondirectory dir)))
    (dolist (lib libraries matched-lib)
      (let ((base (dpd-file--no-load-suffix lib)))
        (when (and (or (not prefix)
                       (> (length base) (length prefix)))
                   (string-prefix-p base key))
          (setq prefix base matched-lib (expand-file-name lib dir)))))))

(defun dpd-fuzzy-recognize (dir)
  "Recognize the main library of DIR using slightly fuzzy matching rules."
  (let* ((libraries (packed-libraries dir nil t))
         (file (dpd-fuzzy-filter dir libraries)))
    (and file (dpd--default-package-for-file file dir))))

(defun dpd-recognize-auctex (dir)
  "Check if DIR contains AUCTeX by reading tex-site.el (if it exists)."
  (when (string-prefix-p "auctex" (file-name-base dir))
    (let ((f (expand-file-name "tex-site.el" dir)))
      (when (file-exists-p f)
        (lm-with-file f
          (package-desc-create
           :name 'auctex
           :version
           (progn
             (search-forward-regexp "(defconst AUCTeX-version \"\\(.*\\)\"")
             (version-to-list (match-string 1)))
           :dir dir))))))

(provide 'dpd)

;; Local Variables:
;; eval: (put 'lm-with-file 'lisp-indent-function 1)
;; End:
